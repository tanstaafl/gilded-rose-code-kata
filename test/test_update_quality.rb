require 'minitest/autorun'
require 'minitest/rg'

require_relative '../gilded_rose.rb'
require_relative '../constants.rb'

module HelperMethods
	def generate_item(name: Items::REGULAR.sample, sell_in: 10, quality: 20)
		@items ||= [] 
		@items << Item.new(name, sell_in, quality)
	end
	
	def item
		@items.first
	end
end

class TestNormalItem < Minitest::Test
	include HelperMethods
	
	def test_degrades_by_1_before_sell_by
		generate_item
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 19, item.quality
	end
	
	def test_degrades_by_2_on_sell_by
		generate_item sell_in: 0
		
		update_quality @items
		
		assert_equal -1, item.sell_in
		assert_equal 18, item.quality
	end
	
	def test_degrades_by_2_after_sell_by
		generate_item sell_in: -10
		
		update_quality @items
		
		assert_equal -11, item.sell_in
		assert_equal 18, item.quality
	end
	
	def test_stops_degrading_when_quality_is_0
		generate_item quality: 0
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 0, item.quality
	end
	
	def test_does_not_go_negative_when_degrading_by_2
		generate_item sell_in: -10, quality: 1
		
		update_quality @items
		
		assert_equal -11, item.sell_in
		assert_equal 0, item.quality
	end
end

class TestAgedBrie < Minitest::Test
	include HelperMethods
	
	def generate_brie(**args)
		generate_item name: Items::AGED_BRIE, **args
	end
	
	def test_upgrades_by_1_before_sell_by
		generate_brie
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 21, item.quality
	end
	
	def test_upgrades_by_2_on_sell_by
		generate_brie sell_in: 0
		
		update_quality @items
		
		assert_equal -1, item.sell_in
		assert_equal 22, item.quality
	end
	
	def test_upgrades_by_2_after_sell_by
		generate_brie sell_in: -10
		
		update_quality @items
		
		assert_equal -11, item.sell_in
		assert_equal 22, item.quality
	end
	
	def test_upgrades_when_quality_is_0
		generate_brie quality: 0
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 1, item.quality
	end
	
	def test_stops_upgrading_when_quality_is_50
		generate_brie quality: 50
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 50, item.quality
	end
	
	def test_does_not_go_over_50_when_upgrading_by_2
		generate_brie sell_in: -10, quality: 49
		
		update_quality @items
		
		assert_equal -11, item.sell_in
		assert_equal 50, item.quality
	end
end

class TestSulfuras < Minitest::Test
	include HelperMethods
	
	def test_never_ages_never_degrades
		generate_item name: Items::SULFURAS, sell_in: 0, quality: 80
		
		update_quality @items
		
		assert_equal 0, item.sell_in
		assert_equal 80, item.quality
	end
end

class TestBackstagePasses < Minitest::Test
	include HelperMethods
	
	def generate_passes(**args)
		generate_item name: Items::BACKSTAGE_PASSES_TO_ETC, **args
	end
	
	def test_upgrades_by_1_when_more_than_10_days_before_sell_by
		generate_passes sell_in: 20
		
		update_quality @items
		
		assert_equal 19, item.sell_in
		assert_equal 21, item.quality
	end
	
	def test_upgrades_by_2_when_10_days_before_sell_by
		generate_passes sell_in: 10
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 22, item.quality
	end
	
	def test_upgrades_by_3_when_5_days_before_sell_by
		generate_passes sell_in: 5
		
		update_quality @items
		
		assert_equal 4, item.sell_in
		assert_equal 23, item.quality
	end
	
	def test_upgrades_by_3_when_1_day_before_sell_by
		generate_passes sell_in: 1
		
		update_quality @items
		
		assert_equal 0, item.sell_in
		assert_equal 23, item.quality
	end
	
	def test_drops_to_0_on_sell_by
		generate_passes sell_in: 0
		
		update_quality @items
		
		assert_equal -1, item.sell_in
		assert_equal 0, item.quality
	end
	
	def test_stops_upgrading_when_quality_is_50
		generate_passes quality: 50
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 50, item.quality
	end
	
	def test_does_not_go_over_50_when_upgrading_by_3
		generate_passes sell_in: 3, quality: 49
		
		update_quality @items
		
		assert_equal 2, item.sell_in
		assert_equal 50, item.quality
	end
end

class TestConjuredItem < Minitest::Test
	include HelperMethods
	
	def generate_conjured_item(**args)
		generate_item name: "#{Items::CONJURED} Mana Cake", **args
	end
	
	def test_degrades_by_2_before_sell_by
		generate_conjured_item
		
		update_quality @items
		
		skip
		assert_equal 9, item.sell_in
		assert_equal 18, item.quality
	end
	
	def test_degrades_by_4_on_sell_by
		generate_conjured_item sell_in: 0
		
		update_quality @items
		
		skip
		assert_equal -1, item.sell_in
		assert_equal 16, item.quality
	end
	
	def test_degrades_by_4_after_sell_by
		generate_conjured_item sell_in: -10
		
		update_quality @items
		
		skip
		assert_equal -11, item.sell_in
		assert_equal 16, item.quality
	end
	
	def test_stops_degrading_when_quality_is_0
		generate_conjured_item quality: 0
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 0, item.quality
	end
	
	def test_does_not_go_negative_when_degrading_by_2
		generate_conjured_item quality: 1
		
		update_quality @items
		
		assert_equal 9, item.sell_in
		assert_equal 0, item.quality
	end
	
	def test_does_not_go_negative_when_degrading_by_4
		generate_conjured_item sell_in: -10, quality: 1
		
		update_quality @items
		
		assert_equal -11, item.sell_in
		assert_equal 0, item.quality
	end
end
