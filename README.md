# 🌹 The Gilded Rose code kata

This is a variant of the Ruby version of Gilded Rose.

The [original C# version](https://github.com/NotMyself/GildedRose) was designed by
[Terry Hughes](https://twitter.com/TerryHughes) and made famous by
[Bobby Johnson's article](http://iamnotmyself.com/2011/02/13/refactor-this-the-gilded-rose-kata/),
which unfortunately got buried under the sands of time (though
[Wayback Machine](https://web.archive.org/web/20200917095950/https://iamnotmyself.com/2011/02/14/refactor-this-the-gilded-rose-kata/)
can help).

This variant in particular copied code from
[Ken Mayer's fork](https://github.com/kmayer/gilded_rose_kata) of
[Jim Weirich's Ruby version](https://github.com/jimweirich/gilded_rose_kata),
including RSpec tests.

Additional information and more languages can be found in
[Emily Bache's repo](https://github.com/emilybache/GildedRose-Refactoring-Kata).

## How to use it

Start with the `main` branch.
First, make sure you have all the necessary gems:

	bundle install

Then, a good intro (or sanity check) is running some tests and gathering metrics:

	bundle exec rspec
	bundle exec flog
	bundle exec ruby test/test_update_quality.rb

By now you should see where the problem lies, and tests should help you ensure
nothing gets broken as you refactor!

Note: there are some skipped tests for the "required update" re: _conjured_ items.
See towards the end of [⏬ original description](#gilded-rose-original-description).
Feel free to unskip them as soon as you are ready!

## Changes from the original

* `update_quality` method signature slightly different,
* RSpec tests added,
* minitest tests added,
* .gitlab-ci.yml runs CI and reports results per test case.

## bin/demo.rb

In the bin/ directory you will find a small demo program creating a bunch of items
and iterating `#update_quality` over them. You can run it like this:

	bin/demo.rb --wide

There are some limited commandline options, including number of iterations.
To view them, run:

	bin/demo.rb --help

# Gilded Rose - original description

Hi and welcome to team Gilded Rose. As you know, we are a small inn
with a prime location in a prominent city run by a friendly innkeeper
named Allison. We also buy and sell only the finest
goods. Unfortunately, our goods are constantly degrading in quality as
they approach their sell by date. We have a system in place that
updates our inventory for us. It was developed by a no-nonsense type
named Leeroy, who has moved on to new adventures. Your task is to add
the new feature to our system so that we can begin selling a new
category of items. First an introduction to our system:

- All items have a SellIn value which denotes the number of days we
  have to sell the item
- All items have a Quality value which denotes how valuable the item is
- At the end of each day our system lowers both values for every item

Pretty simple, right? Well this is where it gets interesting:

- Once the sell by date has passed, Quality degrades twice as fast
- The Quality of an item is never negative
- "Aged Brie" actually increases in Quality the older it gets
- The Quality of an item is never more than 50
- "Sulfuras", being a legendary item, never has to be sold or
  decreases in Quality
- "Backstage passes", like aged brie, increases in Quality as it's
  SellIn value approaches; Quality increases by 2 when there are 10
  days or less and by 3 when there are 5 days or less but Quality
  drops to 0 after the concert

We have recently signed a supplier of conjured items.
This requires an update to our system:

- "Conjured" items degrade in Quality twice as fast as normal items

Feel free to make any changes to the UpdateQuality method and add any
new code as long as everything still works correctly. However, do not
alter the Item class or Items property as those belong to the goblin
in the corner who will insta-rage and one-shot you as he doesn't
believe in shared code ownership (you can make the UpdateQuality
method and Items property static if you like, we'll cover for
you). Your work needs to be completed by Friday,
February 18, 2011 08:00:00 AM PST.

Just for clarification, an item can never have its Quality increase
above 50, however "Sulfuras" is a legendary item and as such its
Quality is 80 and it never alters.
