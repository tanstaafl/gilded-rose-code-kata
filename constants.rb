module Items
	REGULAR = ['+5 Dexterity Vest', 'Elixir of the Mongoose']
	AGED_BRIE = 'Aged Brie'
	SULFURAS = 'Sulfuras, Hand of Ragnaros'
	BACKSTAGE_PASSES = 'Backstage passes'
	BACKSTAGE_PASSES_TO_ETC = "#{BACKSTAGE_PASSES} to a TAFKAL80ETC concert"
	CONJURED = 'Conjured'
end
