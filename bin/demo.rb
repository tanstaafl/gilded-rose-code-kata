#!/usr/bin/env ruby

require 'colorize'

require_relative '../gilded_rose.rb'
require_relative '../constants.rb'

module GildedRose
	class Demo
		def self.create_items
			[
				Item.new('+5 Dexterity Vest', 10, 20),
				Item.new(Items::AGED_BRIE, 2, 0),
				Item.new('Elixir of the Mongoose', 5, 7),
				Item.new(Items::SULFURAS, 0, 80),
				Item.new(Items::BACKSTAGE_PASSES_TO_ETC, 15, 20),
				Item.new("#{Items::CONJURED} Mana Cake", 3, 6),
			]
		end
		
		attr_reader :settings
		
		def initialize(items = Demo.create_items)
			@items = items
			@qualities = {}
			@settings = {
				wide: false,
				gap: 2,
				color: true,
				iterations: 20,
			}
		end
		
		def iterate(iterations = @settings[:iterations])
			every_item{ |item| name item }
			every_item{ |item| item.quality }
			iterations.times do
				remember_quality
				update_quality @items
				every_item{ |item| item.quality }
			end
		end
		
		def every_item
			@items.each do |item|
				output = yield item
				print output.to_s.rjust(column_width item).send(color item)
			end
			puts
		end
		
		def name(item)
			@settings[:wide] ? item.name : item.name.split.first
		end
		
		def color(item)
			return :to_s unless @settings[:color] && @qualities[item.name]
			delta = item.quality - @qualities[item.name]
			case delta
			when ..-4
				:magenta
			when -3..-2
				:red
			when -1
				:yellow
			when 0
				:to_s
			when +1
				:green
			when +2
				:blue
			else
				:cyan
			end
		end
		
		def column_width(item)
			name(item).length + @settings[:gap]
		end
		
		def remember_quality
			@items.each{ |item| @qualities[item.name] = item.quality }
		end
		
		def self.run_demo(argv)
			demo = Demo.new
			OptionParser.new do |opts|
				opts.on('-w', '--wide', TrueClass, 'Show full item name as column header (default: first word)')
				opts.on('-g', '--gap GAP', Integer, "Gap between columns (default: #{demo.settings[:gap]})")
				opts.on('-c', '--[no-]color [COLOR]', TrueClass, "Highlight quality changes in different colors (default: #{demo.settings[:color]})")
				opts.on('-i', '--iterations NUM_ITERATIONS', Integer, "How many times to `#update_quality` (default: #{demo.settings[:iterations]})")
			end.parse! into: demo.settings
			demo.iterate
		rescue Exception => exc
			puts exc.class.name.white.on_red
			puts exc.message
			exit -1
		end
	end
end

if $0 == __FILE__
	require 'optparse'
	GildedRose::Demo.run_demo ARGV
end
